describe("Product Details Controller", function () {
    var//iable declarations
        $rootScope,
        $scope,
        $controller,
        ProductDetailsController,
        Product
    ;
    const ANY_ELEMENT = 'insurance';
    const SELECTED_PRODUCT = { defined :  {
        phone: "Iphone",
        contract: {
            months:  24,
            price: 10
        },
        internet: {
            data: 5000,
            price: 10,
            discount: 7
        },
        voice: {
            minutes: 150,
            price: 10,
            discount: 2
        },
        insurance: false,
        connection: 30
    } };

    const SELECTED_ROW_RESULT = 'success';
    const NOT_SELECTED_ROW_RESULT = '';


    beforeEach(function () {
        module('app');
        inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $controller = $injector.get('$controller');
            Product = $injector.get('Product');
        });
    });

    describe("ProductDetails", function () {
        beforeEach(function () {
            $scope = $rootScope.$new();
            Product.product = SELECTED_PRODUCT
            spyOn($rootScope, '$broadcast').andCallThrough();
            ProductDetailsController = $controller('ProductDetails', { $scope: $scope });
        });

        describe("Event Listeners on the $rootScope", function () {
            beforeEach(function () {
                $rootScope.$broadcast('updateProduct');
            });
            it("Should be fired when update cart is received", function () {
                expect($rootScope.$broadcast).toHaveBeenCalledWith('updateProduct');
            });

            it("Should set the variables $scope.product to Product.product", function () {
                expect($scope.product).toBe(Product.product)
            });
        });

        describe("Update cart function", function () {
            it("Should update the product", function () {
                $scope.updateCart();
                expect($rootScope.$broadcast).toHaveBeenCalledWith('updateProduct');
            });
        });

        describe("Selected Row Function", function () {
            beforeEach(function () {
                $scope.updateCart(ANY_ELEMENT, SELECTED_PRODUCT.defined.voice);
            });

            it("Should return no class if there isnt any row selected", function () {
                delete $scope.product;
                expect($scope.selectedRow(ANY_ELEMENT, SELECTED_PRODUCT.defined.internet)).toBe(NOT_SELECTED_ROW_RESULT);
            });

            it("Should return the correct class depending on the selected row", function () {
                expect($scope.selectedRow(ANY_ELEMENT, SELECTED_PRODUCT.defined.voice)).toBe(SELECTED_ROW_RESULT);
            });

            it("Should return no class if the row is not selected.", function () {
                expect($scope.selectedRow(ANY_ELEMENT, SELECTED_PRODUCT.defined.internet)).toBe(NOT_SELECTED_ROW_RESULT);
            });
        });
    });


});