app.controller('ProductDetails', function ($scope, Product) {

    $scope.$on('updateProduct', function () {
        $scope.product = Product.product;
    });

    $scope.updateCart = function(element, data) {
        Product.updateProduct(element, { data: data });
    };

    $scope.selectedRow = function(element, product) {
         return ($scope.product && product === $scope.product.defined[element]) ? 'success' : '';
    };

});