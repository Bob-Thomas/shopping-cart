
app.controller('ShoppingCart', function($rootScope, $scope, Product) {

    //Listeners on the $rootScope
    $rootScope.$on('updateProduct', function() {
        /**
         * Event listener for when the product in the Product Service is updated.
         */
        //Define the product on the controllers scope.
        $scope.product = Product.product.defined;

        //Define and calculate the corresponding figures.
        $scope.total = $scope.product.contract.price + $scope.product.internet.price + $scope.product.voice.price;
        $scope.discount = $scope.product.internet.discount + $scope.product.voice.discount;
        $scope.discountedPrice = $scope.total - $scope.discount;
    });

});