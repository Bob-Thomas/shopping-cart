describe("Shopping Cart Controller", function() {
    var//iable declarations
        $rootScope,
        $scope,
        $controller,
        ShoppingCartController,
        Product
    ;

    beforeEach( function() {
        module('app');
        inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $controller = $injector.get('$controller');
            Product = $injector.get('Product');
        });
    });

    describe('ShoppingCart', function() {

        beforeEach(function() {
            $scope = $rootScope.$new();
            Product.product = { defined :  {
                phone: "Iphone",
                contract: {
                    months:  24,
                    price: 10
                },
                internet: {
                    data: 5000,
                    price: 10,
                    discount: 7

                },
                voice: {
                    minutes: 150,
                    price: 10,
                    discount: 2
                },
                insurance: false,
                connection: 30
                }
            };

            spyOn($rootScope, '$broadcast').andCallThrough();
            ShoppingCartController = $controller('ShoppingCart', { $scope: $scope });
        });

        describe('Event Listeners on the $rootScope', function () {
            beforeEach(function() {
                $rootScope.$broadcast('updateProduct');
            });

            it('Should be fired when updateCart is received', function () {
                expect($scope.product).toBe(Product.product.defined);
                expect($rootScope.$broadcast).toHaveBeenCalledWith('updateProduct');

            });
            it('Should update the local scope variables', function() {
                expect($scope.total).toBe($scope.product.contract.price + $scope.product.internet.price + $scope.product.voice.price);
                expect($scope.discount).toBe($scope.product.internet.discount + $scope.product.voice.discount);
                expect($scope.discountedPrice).toBe($scope.total - $scope.discount);

            })
        });
    });

});